package main

import (
	"testing"
)

func Test_getRepository(t *testing.T) {
	r, err := getRepository("TestData/repository.json")

	if err != nil {
		t.Error(err.Error())
		return
	}
	if r.Owner.Login != "apache" {
		t.Error("Wrong owner")
	}
	if r.Name != "kafka" {
		t.Error("Wrong name")
	}
}

func Test_getTopContributor(t *testing.T) {
	data := projectInfo{}
	err := getTopContributor(&data, "TestData/contributors.json")

	if err != nil {
		t.Error(err.Error())
		return
	}
	if data.Committer != "ijuma" {
		t.Error("Wrong contributor")
	}
	if data.Commits != 310 {
		t.Error("Wrong number of contributions")
	}
}

func Test_getLanguages(t *testing.T) {
	data := projectInfo{}
	err := getLanguages(&data, "TestData/languages.json")

	if err != nil {
		t.Error(err.Error())
		return
	}

	if len(data.Languages) != 7 {
		t.Error("Wrong number of languages")
		return
	}

	if data.Languages[0] != "Batchfile" || data.Languages[6] != "XSLT" {
		t.Error("Wrong languages or languages are not sorted")
	}
}
