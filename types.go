package main

type projectInfo struct {
	Project   string   `json:"project"`
	Owner     string   `json:"owner"`
	Committer string   `json:"committer"`
	Commits   int      `json:"commits"`
	Languages []string `json:"language"`
}

type contributor struct {
	Login         string `json:"login"`
	Contributions int    `json:"contributions"`
}

type languages map[string]interface{}

type repository struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	FullName string `json:"full_name"`
	Owner    struct {
		Login string `json:"login"`
	} `json:"owner"`
	LanguagesURL    string `json:"languages_url"`
	ContributorsURL string `json:"contributors_url"`
}
