package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"sort"
	"strings"
)

func main() {
	http.HandleFunc("/projectinfo/v1/", handlerInfo)
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	fmt.Println(port)
	http.ListenAndServe(":"+port, nil)
}

func handlerInfo(w http.ResponseWriter, r *http.Request) {

	// Parse the URL
	parts := strings.Split(r.URL.Path, "/")

	if !(len(parts) == 6 || len(parts) == 7 && parts[6] == "") {
		http.Error(w, http.StatusText(400), 400)
		return
	}

	// Get the project info
	data, code := getProjectInfo(w, parts)

	// Check errors
	if code != http.StatusOK {
		http.Error(w, http.StatusText(code), code)
		return
	}

	// Set header and return the data
	http.Header.Add(w.Header(), "content-type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func getProjectInfo(w http.ResponseWriter, parts []string) (data projectInfo, code int) {

	// Get the repository data
	repo, err := getRepository("https://api.github.com/repos/" + parts[4] + "/" + parts[5])
	if err != nil {
		code = http.StatusInternalServerError
		return
	}

	if repo.ID == 0 {
		code = http.StatusNotFound
		return
	}

	data.Owner = repo.Owner.Login
	data.Project = repo.Name

	// Get the top contributor and languages
	errC := getTopContributor(&data, repo.ContributorsURL)
	errL := getLanguages(&data, repo.LanguagesURL)

	if errC != nil || errL != nil {
		code = http.StatusInternalServerError
		return
	}

	code = http.StatusOK
	return
}

func getRepository(url string) (repo repository, e error) {
	ior, e := request(url)
	if e != nil {
		return
	}

	e = json.NewDecoder(ior).Decode(&repo)

	if e != nil {
		fmt.Println(e.Error())
		return
	}

	return
}

func getTopContributor(data *projectInfo, url string) (e error) {
	ior, e := request(url)
	if e != nil {
		return
	}

	c := make([]contributor, 0)
	e = json.NewDecoder(ior).Decode(&c)

	if e != nil {
		fmt.Println(e.Error())
		return
	}

	if len(c) > 0 {
		data.Committer = c[0].Login
		data.Commits = c[0].Contributions
	}

	return
}

func getLanguages(data *projectInfo, url string) (e error) {
	ior, e := request(url)
	if e != nil {
		return
	}

	l := make(languages)
	e = json.NewDecoder(ior).Decode(&l)

	if e != nil {
		fmt.Println(e.Error())
		return
	}

	for key := range l {
		data.Languages = append(data.Languages, key)
	}

	sort.Strings(data.Languages)

	return
}

func request(url string) (ior io.Reader, e error) {
	parts := strings.Split(url, "/")
	if parts[0] == "TestData" {
		ior, e = requestLocal(url)
	} else {
		ior, e = requestURL(url)
	}
	return
}

func requestURL(url string) (ior io.Reader, e error) {
	r, e := http.Get(url)

	if e != nil {
		fmt.Println(e.Error())
		return
	}

	ior = r.Body
	return
}

func requestLocal(url string) (ior io.Reader, e error) {
	byteData, _ := ioutil.ReadFile(url)
	if byteData != nil {
		ior = bytes.NewReader(byteData)
	}
	return
}
